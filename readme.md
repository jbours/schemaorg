# Schema.org tag generator

http://schema.org

https://ludwig.im/en/projects/html5-and-schema.org-seo-best-practices-for-microdata

usage: (in this example i used the full qualified class name for the classes, in real live situations you probably import them.)

itemtype:
`<?php echo \JBours\SchemaOrg\Generate::typeTag(JBours\SchemaOrg\Type\<ClassName>:class); ?>`

itemproperty:
`<?php echo \JBours\SchemaOrg\Generate::propertyTag(JBours\SchemaOrg\Property\<ClassName>:class); ?>`

itemscope:
`<?php echo \JBours\SchemaOrg\Generate::itemScopeTag(); ?>`

real live example:
`<?php echo Generate::propertyTag(new Name()) ?>`
Generates:
`itemproperty="name"`
