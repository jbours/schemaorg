<?php

namespace JBours\SchemaOrg;

use JBours\SchemaOrg\Property\InteractionCount;
use Spatie\SchemaOrg\Person;
use Spatie\SchemaOrg\Thing;

require_once 'vendor/autoload.php';

$thing = new Thing();
$thing->description("lorem ipsum dolar")
      ->sameAs('ipsum lorem');
echo var_dump($thing->toArray());

echo Generate::typeTag(new Person()) . PHP_EOL;
echo Generate::propertyTag(new InteractionCount()) . PHP_EOL;
echo Generate::itemScopeTag() . PHP_EOL;