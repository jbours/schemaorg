<?php

namespace JBours\SchemaOrg\Tests;

use JBours\SchemaOrg\Generate;
use JBours\SchemaOrg\Property\Name;
use PHPUnit\Framework\TestCase;
use Spatie\SchemaOrg\Person;

class GeneratorTest extends TestCase
{
    public function testItCanGenerateAPropertyTag(): void
    {
        $expected = 'itemprop="name"';
        $propertyTag = Generate::propertyTag(new Name());

        self::assertEquals($expected, $propertyTag);
    }

    public function testItCanGenerateATypeTag(): void
    {
        $expected = 'itemtype="http://schema.org/Person"';
        $propertyTag = Generate::typeTag(new Person());

        self::assertEquals($expected, $propertyTag);
    }

    public function testItCanGenerateAScopeTag(): void
    {
        $expected = 'itemscope';
        $propertyTag = Generate::itemScopeTag();

        self::assertEquals($expected, $propertyTag);
    }
}
