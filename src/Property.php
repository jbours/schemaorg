<?php

namespace JBours\SchemaOrg;

interface Property
{
    public function __toString(): string;
}
