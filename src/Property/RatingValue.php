<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class RatingValue implements Property
{
    public function __toString(): string
    {
        return 'ratingValue';
    }
}
