<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Publisher implements Property
{
    public function __toString(): string
    {
        return 'publisher';
    }
}
