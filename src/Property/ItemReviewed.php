<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ItemReviewed implements Property
{
    public function __toString(): string
    {
        return 'itemreviewed';
    }
}
