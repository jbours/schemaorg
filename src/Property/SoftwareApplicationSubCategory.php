<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class SoftwareApplicationSubCategory implements Property
{
    public function __toString(): string
    {
        return 'softwareApplicationSubCategory';
    }
}
