<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Caption implements Property
{
    public function __toString(): string
    {
        return 'caption';
    }
}
