<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class HasPart implements Property
{
    public function __toString(): string
    {
        return 'hasPart';
    }
}
