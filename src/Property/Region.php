<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Region implements Property
{
    public function __toString(): string
    {
        return 'region';
    }
}
