<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Events implements Property
{
    public function __toString(): string
    {
        return 'events';
    }
}
