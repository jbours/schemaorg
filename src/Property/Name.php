<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Name implements Property
{
    public function __toString(): string
    {
        return 'name';
    }
}
