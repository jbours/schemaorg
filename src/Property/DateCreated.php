<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class DateCreated implements Property
{
    public function __toString(): string
    {
        return 'dateCreated';
    }
}
