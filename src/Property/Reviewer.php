<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Reviewer implements Property
{
    public function __toString(): string
    {
        return 'reviewer';
    }
}
