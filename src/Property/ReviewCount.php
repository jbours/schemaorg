<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ReviewCount implements Property
{
    public function __toString(): string
    {
        return 'reviewCount';
    }
}
