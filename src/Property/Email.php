<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Email implements Property
{
    public function __toString(): string
    {
        return 'email';
    }
}
