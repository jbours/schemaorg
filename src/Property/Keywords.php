<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Keywords implements Property
{
    public function __toString(): string
    {
        return 'keywords';
    }
}
