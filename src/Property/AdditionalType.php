<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class AdditionalType implements Property
{
    public function __toString(): string
    {
        return 'additionalType';
    }
}
