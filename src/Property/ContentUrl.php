<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ContentUrl implements Property
{
    public function __toString(): string
    {
        return 'contextUrl';
    }
}
