<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class InteractionCount implements Property
{
    public function __toString(): string
    {
        return 'interactionCount';
    }
}
