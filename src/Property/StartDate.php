<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class StartDate implements Property
{
    public function __toString(): string
    {
        return 'startDate';
    }
}
