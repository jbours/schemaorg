<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class AddressCountry implements Property
{
    public function __toString(): string
    {
        return 'addressCountry';
    }
}
