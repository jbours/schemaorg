<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ArticleBody implements Property
{
    public function __toString(): string
    {
        return 'articleBody';
    }
}
