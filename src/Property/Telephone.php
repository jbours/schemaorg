<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Telephone implements Property
{
    public function __toString(): string
    {
        return 'telephone';
    }
}
