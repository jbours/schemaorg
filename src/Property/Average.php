<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Average implements Property
{
    public function __toString(): string
    {
        return 'average';
    }
}
