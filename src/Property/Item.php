<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Item implements Property
{
    public function __toString(): string
    {
        return 'item';
    }
}
