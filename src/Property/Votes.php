<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Votes implements Property
{
    public function __toString(): string
    {
        return 'votes';
    }
}
