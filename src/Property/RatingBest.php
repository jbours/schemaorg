<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class RatingBest implements Property
{
    public function __toString(): string
    {
        return 'ratingBest';
    }
}
