<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class BestRating implements Property
{
    public function __toString(): string
    {
        return 'bestRating';
    }
}
