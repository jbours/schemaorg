<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class AlternativeHeadline implements Property
{
    public function __toString(): string
    {
        return 'alternativeHeadline';
    }
}
