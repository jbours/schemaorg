<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ThumbnailUrl implements Property
{
    public function __toString(): string
    {
        return 'thumbnail';
    }
}
