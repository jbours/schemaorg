<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Country implements Property
{
    public function __toString(): string
    {
        return 'country';
    }
}
