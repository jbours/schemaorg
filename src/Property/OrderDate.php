<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class OrderDate implements Property
{
    public function __toString(): string
    {
        return 'orderDate';
    }
}
