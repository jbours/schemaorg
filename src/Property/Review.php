<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Review implements Property
{
    public function __toString(): string
    {
        return 'review';
    }
}
