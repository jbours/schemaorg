<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Creator implements Property
{
    public function __toString(): string
    {
        return 'creator';
    }
}
