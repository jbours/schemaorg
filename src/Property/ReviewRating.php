<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ReviewRating implements Property
{
    public function __toString(): string
    {
        return 'reviewRating';
    }
}
