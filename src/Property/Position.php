<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Position implements Property
{
    public function __toString(): string
    {
        return 'position';
    }
}
