<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class MainContentOfPage implements Property
{
    public function __toString(): string
    {
        return 'mainContentOfPage';
    }
}
