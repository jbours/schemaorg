<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Headline implements Property
{
    public function __toString(): string
    {
        return 'headline';
    }
}
