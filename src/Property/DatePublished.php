<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class DatePublished implements Property
{
    public function __toString(): string
    {
        return 'DatePublished';
    }
}
