<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Price implements Property
{
    public function __toString(): string
    {
        return 'price';
    }
}
