<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Comment implements Property
{
    public function __toString(): string
    {
        return 'comment';
    }
}
