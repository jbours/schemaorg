<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Best implements Property
{
    public function __toString(): string
    {
        return 'best';
    }
}
