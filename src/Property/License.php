<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class License implements Property
{
    public function __toString(): string
    {
        return 'licence';
    }
}
