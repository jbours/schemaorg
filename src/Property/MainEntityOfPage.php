<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class MainEntityOfPage implements Property
{
    public function __toString(): string
    {
        return 'mainEntityOfPage';
    }
}
