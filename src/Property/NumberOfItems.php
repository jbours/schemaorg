<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class NumberOfItems implements Property
{
    public function __toString(): string
    {
        return 'numberOfItems';
    }
}
