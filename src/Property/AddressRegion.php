<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class AddressRegion implements Property
{
    public function __toString(): string
    {
        return 'addressRegion';
    }
}
