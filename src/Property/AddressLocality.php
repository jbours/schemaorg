<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class AddressLocality implements Property
{
    public function __toString(): string
    {
        return 'addressLocality';
    }
}
