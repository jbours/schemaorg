<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Value implements Property
{
    public function __toString(): string
    {
        return 'value';
    }
}
