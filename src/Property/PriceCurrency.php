<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class PriceCurrency implements Property
{
    public function __toString(): string
    {
        return 'priceCurrency';
    }
}
