<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Rating implements Property
{
    public function __toString(): string
    {
        return 'rating';
    }
}
