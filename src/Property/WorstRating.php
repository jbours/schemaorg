<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class WorstRating implements Property
{
    public function __toString(): string
    {
        return 'worstRating';
    }
}
