<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class DiscussionUrl implements Property
{
    public function __toString(): string
    {
        return 'discussionUrl';
    }
}
