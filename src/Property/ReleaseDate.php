<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ReleaseDate implements Property
{
    public function __toString(): string
    {
        return 'releaseDate';
    }
}
