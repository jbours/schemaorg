<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Description implements Property
{
    public function __toString(): string
    {
        return 'description';
    }
}
