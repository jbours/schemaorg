<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Url implements Property
{
    public function __toString(): string
    {
        return 'url';
    }
}
