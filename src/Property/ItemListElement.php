<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ItemListElement implements Property
{
    public function __toString(): string
    {
        return 'itemListElement';
    }
}
