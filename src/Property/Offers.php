<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Offers implements Property
{
    public function __toString(): string
    {
        return 'offers';
    }
}
