<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class AggregateRating implements Property
{
    public function __toString(): string
    {
        return 'aggregateRating';
    }
}
