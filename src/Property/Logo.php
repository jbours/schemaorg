<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Logo implements Property
{
    public function __toString(): string
    {
        return 'logo';
    }
}
