<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class CopyrightYear implements Property
{
    public function __toString(): string
    {
        return 'copyrightYear';
    }
}
