<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class ProgrammingLanguage implements Property
{
    public function __toString(): string
    {
        return 'programmingLanguage';
    }
}
