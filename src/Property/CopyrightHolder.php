<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class CopyrightHolder implements Property
{
    public function __toString(): string
    {
        return 'copyrightHolder';
    }
}
