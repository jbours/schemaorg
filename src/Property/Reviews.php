<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Reviews implements Property
{
    public function __toString(): string
    {
        return 'reviews';
    }
}
