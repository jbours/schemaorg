<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Availability implements Property
{
    public function __toString(): string
    {
        return 'availability';
    }
}
