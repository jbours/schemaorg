<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class CommentTime implements Property
{
    public function __toString(): string
    {
        return 'commentType';
    }
}
