<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Role implements Property
{
    public function __toString(): string
    {
        return 'role';
    }
}
