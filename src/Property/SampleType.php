<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class SampleType implements Property
{
    public function __toString(): string
    {
        return 'sampleType';
    }
}
