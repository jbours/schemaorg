<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class DTReviewed implements Property
{
    public function __toString(): string
    {
        return 'dTReviewed';
    }
}
