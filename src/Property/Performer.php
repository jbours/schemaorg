<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Performer implements Property
{
    public function __toString(): string
    {
        return 'performer';
    }
}
