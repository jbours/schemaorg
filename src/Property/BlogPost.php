<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class BlogPost implements Property
{
    public function __toString(): string
    {
        return 'blogPost';
    }
}
