<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Image implements Property
{
    public function __toString(): string
    {
        return 'image';
    }
}
