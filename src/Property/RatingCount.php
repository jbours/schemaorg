<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class RatingCount implements Property
{
    public function __toString(): string
    {
        return 'ratingCount';
    }
}
