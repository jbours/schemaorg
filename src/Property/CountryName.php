<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class CountryName implements Property
{
    public function __toString(): string
    {
        return 'countryName';
    }
}
