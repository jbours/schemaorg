<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class Author implements Property
{
    public function __toString(): string
    {
        return 'author';
    }
}
