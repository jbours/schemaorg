<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class CommentText implements Property
{
    public function __toString(): string
    {
        return 'commentText';
    }
}
