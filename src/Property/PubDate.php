<?php

namespace JBours\SchemaOrg\Property;

use JBours\SchemaOrg\Property;

class PubDate implements Property
{
    public function __toString(): string
    {
        return 'pubDate';
    }
}
