<?php

namespace JBours\SchemaOrg;

use ReflectionClass;
use ReflectionException;
use Spatie\SchemaOrg\Type;

use function sprintf;

/**
 * @template-covariant T of object
 */
class Generate
{
    /**
     * @var string
     */
    protected static $providerUrl = 'http://schema.org/';

    public static function propertyTag(Property $property): string
    {
        return self::generateProperty($property->__toString());
    }

    /**
     * @param class-string<object>|object $class
     *
     * @throws ReflectionException
     */
    public static function getClassName($class): string
    {
        return (new ReflectionClass($class))->getShortName();
    }

    protected static function generateProperty(string $property): string
    {
        return self::generatePropertyAndValue('itemprop', $property);
    }

    public static function typeTag(Type $type): string
    {
        try {
            $name = self::getClassName($type);

            return self::generateType($name);
        } catch (ReflectionException $e) {
            return '';
        }
    }

    protected static function generateType(string $type): string
    {
        return self::generatePropertyAndValue('itemtype', self::$providerUrl . $type);
    }

    public static function itemScopeTag(): string
    {
        return 'itemscope';
    }

    private static function generatePropertyAndValue(string $property, string $value): string
    {
        return sprintf('%s="%s"', $property, $value);
    }
}
